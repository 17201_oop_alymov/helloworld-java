package ru.nsu.g.avoronkov.helloworld;

import java.io.PrintStream;

public class Hello {
	public void greet(PrintStream out, String someone) {
		out.printf("Hello %s!\n", someone);
	}
}
